﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class Timer : MonoBehaviour {


	public float TimeLeft;
	public Text TimerText;

	// Use this for initialization
	void Start () {
		//  Reset timer to level time
		TimeLeft = 120;
		// reset time text to timer
		TimerText.text = "2 minutes";

	}
	
	// Update is called once per frame
	void Update () {
		TimeLeft -= Time.deltaTime;

		TimerText.text = TimeLeft.ToString () + " Seconds left";

		if (TimeLeft < 0) {
			// game/level end
			//show scores
			// stop play


		}
	}
}
