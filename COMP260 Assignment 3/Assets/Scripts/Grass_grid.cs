﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Grass_grid : MonoBehaviour {

	// set number of grid squares on each axis
	public int x_range = 10;
	public int y_range = 10;

	private Vector2 position;

	public Transform Grass_prefab;


	// Use this for initialization
	void Start () {

		position.x = 0;
		position.y = 0;

		CreateGrid ();
		
	}

	void CreateGrid () {
		for (int x = 0; x < x_range; x++) {
			for (int y = 0; y < y_range; y++) {
				
				Transform Grass = Instantiate (Grass_prefab);
				// attach to player object in the hierarchy
				Grass.transform.parent = transform;            
				// give the flame a name and number
				Grass.gameObject.name = "GrassTile_" + x;



				// renew init vector for addition
				Vector2 grass_pos = new Vector2 (position.x, position.y);

				//give flame a position
				Grass.transform.localPosition = new Vector2 (grass_pos.x + x, grass_pos.y + y);
			}
		}
	}
	
	// Update is called once per frame
	void Update () {
		
	}


}
