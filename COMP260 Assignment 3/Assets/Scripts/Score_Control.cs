﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Score_Control : MonoBehaviour {

	static private Score_Control instance;
	static public Score_Control Instance {
		get { return instance; }
	}

	private int Score;
	public int pointsPerEnemy = 10;
	public int pointsPerFlame = 50;

	public int EnemiesDefeated;


	// Use this for initialization
	void Start () {
		Score = 0;
		if (instance == null) {
			// save this instance
			instance = this;
		} else {
			// more than one instance exists
			Debug.LogError(
				"More than one Scorekeeper exists in the scene.");
		}            
	} 


	
	// Update is called once per frame
	void Update () {
		
	}

	public void OnDefeatEnemy() {
		Score += pointsPerEnemy;

	}



}
