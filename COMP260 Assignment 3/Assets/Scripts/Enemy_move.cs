﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class Enemy_move : MonoBehaviour {

	public float speed = 0.5f;
	public Rigidbody2D target;
	private Rigidbody2D rigidbody;

	public LayerMask player_flame;
	public Player_move player;

	public LayerMask Player_Layer;

	public Score_Control score_control;

	// Use this for initialization
	void Start () {

		// find the player object to be the target by type
		Player_move player = FindObjectOfType<Player_move>();
		target = player.GetComponent<Rigidbody2D> ();

		rigidbody = GetComponent<Rigidbody2D>();


		player = GetComponent<Player_move> ();


		score_control = GetComponent<Score_Control> ();


	}
	
	// Update is called once per frame
	void FixedUpdate () {
		// get the vector from the enemy to the player target
		// and normalise it
		Vector2 direction = target.position - (Vector2)transform.position;
		direction = direction.normalized;

		Vector2 velocity = direction * speed;

		transform.Translate(velocity * Time.deltaTime);
	}

	void OnCollisionEnter2D (Collision2D collision) {
		if (player_flame.Contains(collision.gameObject)) {
			
			//score_control.Score += 1;
			Destroy (gameObject);
			}


		}


	}

