﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Generate_tail : MonoBehaviour {

	public Transform flame_prefab;
	private float init_x = -0.8f;
	public float flame_space = 0.5f;
	public Player_move player;

	// Use this for initialization
	void Start () {
		player = GetComponent<Player_move> ();
		int initlen_flame = player.len_flame;
		// instantiate the no. of flames for the beginning
		for(int x = 0; x < initlen_flame; x++){
			Transform flame = Instantiate(flame_prefab);
			// attach to player object in the hierarchy
			flame.transform.parent = transform;            
			// give the flame a name and number
			flame.gameObject.name = "Flame " + x;

			//give flame a position
			flame.transform.position = new Vector2((init_x - x), 0);
		}
	}
		// Update is called once per frame
	void Update () {

		// if the no. of current, initiated flames is not the same as the no. the player holds
		if (player.transform.childCount != player.len_flame) {
			// destroy all current flames & renew (can't think of a better way right now)

			foreach (Transform child in player.transform) {
				GameObject.Destroy (child.gameObject);

			}

			// now re-instantiate the no. of flames recquired
			for (int x = 0; x < player.len_flame; x++) {
				Transform flame = Instantiate (flame_prefab);
				// attach to player object in the hierarchy
				flame.transform.parent = transform;            
				// give the flame a name and number
				flame.gameObject.name = "Flame " + x;

				//give flame a position
				flame.transform.localPosition = new Vector2 ((init_x - x), 0);
			}
		}

	}
}
