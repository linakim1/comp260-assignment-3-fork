﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class Player_move : MonoBehaviour {

	private Rigidbody2D rigidbody;

	private float x_dir;
	private float y_dir;
	public int len_flame = 2;
	public float speed = 1f;


	public LayerMask Enemy_Layer;
	public LayerMask PowerUp_Layer;
	public LayerMask Grass_Layer;


	// Use this for initialization
	void Start () {
		rigidbody = GetComponent<Rigidbody2D>();
	}
	
	// Update is called once per frame
	void FixedUpdate () {


		// Get input for keyboard
		x_dir = Input.GetAxis("Horizontal");
		y_dir = Input.GetAxis("Vertical");

		// add input to direction 
		//direction.x += x_dir;
	
		//direction.y += y_dir;

		Vector2 direction = new Vector2(x_dir, y_dir);

		Vector2 velocity = direction * speed;


		// move the object
		transform.Translate(velocity * Time.deltaTime);

		
	}

	void OnCollisionEnter2D (Collision2D collision) {
		// check what has been hit
		if (Enemy_Layer.Contains (collision.gameObject)) {
			Debug.Log ("hit enemy");
			// hit an enemy and lose a flame
			len_flame -= 1;
			Debug.Log (len_flame);

		}
	}

	void OnTriggerEnter2D (Collider2D collider) {
		if (PowerUp_Layer.Contains (collider.gameObject)) {
			// Hit powerup object and collect a flame
			Debug.Log ("hit powerUp");
			len_flame += 1;
			DestroyObject (collider.gameObject);

		}
			

	}
		

	void GameOver () {
		if (len_flame < 1) {
			// health is 0
			Debug.Log("game over");


		}


	}
		
}
