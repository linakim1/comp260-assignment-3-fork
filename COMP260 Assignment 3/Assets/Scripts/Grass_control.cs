﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Grass_control : MonoBehaviour {

	public LayerMask Player_Layer;
	public GameObject BurntGrass_prefab;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnTriggerExit2D(Collider2D collider) {
		if (Player_Layer.Contains (collider.gameObject)) {
			int NoBurnt = transform.childCount;
			// Instantiate grass burn prefab
			GameObject BurntGrass = Instantiate(BurntGrass_prefab);
			// attach to player object in the hierarchy
			BurntGrass.transform.parent = transform;            
			// give the flame a name and number
			BurntGrass.gameObject.name = "BurntGrass " + (NoBurnt + 1);

			//give flame a position
			BurntGrass.transform.position = (Vector2)collider.attachedRigidbody.position;

			GenerateObject ();
		}


	}

	void GenerateObject () {
		// Decide whether to generate an item


	}
}
